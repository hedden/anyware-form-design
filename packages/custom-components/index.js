import example from './example/example.vue'

const components = [
    example
];

const CustomComponents = {
    install (Vue) {
        if (this.installed) return;
        this.installed = true;
        components.map(component => {
            Vue.component(component.name, component);
        })
    }
};

export default CustomComponents